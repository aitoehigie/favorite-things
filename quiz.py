from cryptography.fernet import Fernet

key = 'TluxwB3fV_GWuLkR1_BzGs1Zk90TYAuhNMZP_0q4WyM='


# Oh no! The code is going over the edge! What are you going to do?
message = b'gAAAAABdTMhL74xahkn3QaiMijB3J-dcE-dTHvlCOBf0Q1QLMrLd6UMsBI1KbzaU4Jb5KZNEKDcx8cyTTvb-A7pLQDAeAG75kzOcxWDTSZZ2rBGqS-P8JMNzWVu5NWvUj-veDf31HlR6xB5T4oh_WaL8sUpG-1BDcLidsbQCU1V7lPPsrtf5NBo='


def main():
    f = Fernet(key)
    print(f.decrypt(message))


if __name__ == "__main__":
    main()
